import turtle

t = turtle.Turtle()

penSquare = input("Enter pen color of the square: ")
fillSquare = input("Enter fill color of the square: ")
penCircle = input("Enter pen color of the circle: ")
fillCircle = input("Enter fill color of the circle: ")



t.pensize(3)

t.fillcolor(fillSquare)
t.pencolor(penSquare)


t.begin_fill()

for i in range(4):
    t.forward(100)
    t.left(90)
t.end_fill()

t.penup()
t.backward(200)
t.pendown()


t.fillcolor(fillCircle)
t.pencolor(penCircle)

t.begin_fill()

t.circle(40)

t.end_fill()
