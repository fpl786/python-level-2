'''
Build a simple calculator program in python

Ask the user for the first number

Ask the user for the operation (+, -, *, /)

Ask the user for the second number

Print the result

'''

number1 = int(input("Enter the first number: "))

operation = input("Enter the operation: ")

number2 = int(input("Enter the second number: "))

if operation == "+":
    print(number1,"+", number2,"=", number1+number2)
elif operation == "-":
    print(number1,"-", number2,"=", number1-number2)
elif operation == "*":
    print(number1,"*", number2,"=", number1*number2)
elif operation == "/":
    print(number1,"/", number2,"=", number1/number2)
else:
    print("Invalid Operation")
