import random


number = random.randint(1,20)
print(number)

guess = int(input("Guess the number: "))

if guess != number:
    print("Incorrect! Please Try Again")
    guess = int(input("Guess the number: "))
    if guess != number:
        print("Incorrect! Please Try Again")
        guess = int(input("Guess the number: "))
        if guess != number:
            print("You Lose! The correct answer is", number)
        else:
            print("You Win")
    else:
        print("You Win!") 
else:
    print("You Win!")
