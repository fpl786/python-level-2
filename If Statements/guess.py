'''

build a simple guessing game
generate random number from 1 to 20
Ask the user to guess the number
if the number is correct  print "You Win"
otherwise print "You Lose"


HINT: import the random module



Challenge: modify this program to allow the user to
guess 3 times instead of once. You cannot use any loops.
If the user gets it right, the program should not ask for
any more guesses.

HINT: Use Nested If Statements


'''
import random


number = random.randint(1,20)


guess = int(input("Guess the number: "))


if guess == number:
    print("You Win!")
else:
    print("You Lose! The correct answer is", number)

