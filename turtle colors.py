
'''

draw 3 shapes

square

circle

dot

Each shape should have a different pencolor and fill color

'''
import turtle

t = turtle.Turtle()

t.pensize(3)

t.fillcolor("green")
t.pencolor("blue")


t.begin_fill()

for i in range(4):
    t.forward(100)
    t.left(90)
t.end_fill()

t.penup()
t.backward(200)
t.pendown()


t.fillcolor("red")
t.pencolor("purple")

t.begin_fill()

t.circle(40)

t.end_fill()

t.pencolor("deep pink")

t.penup()
t.forward(100)
t.pendown()

t.dot(30)


