'''

write a python program that takes in the number of minutes as input
and converts it to seconds and prints result

'''

minutes = int(input("Enter the number of minutes: "))

print(minutes, "minutes is", minutes*60, "seconds")
