
'''
Ask the user to enter a number
print a pyramid like below with that
number of rows

Hint: You need a for loop
      Use the string replication (*) operator for strings

Eg. 1

Enter a number: 5

*
**
***
****
*****

Eg. 2

Enter a number: 7

*
**
***
****
*****
******
*******

'''
# range(1,21) -> 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20



number = int(input("Enter a number: "))

for i in range(1, number + 1):
    print("*"* i)
    




